//***************************
//Honor Code: The work we are submitting is the result of our own thinking and efforts.
//Shick, Brindle
//Lab 6
//10/19/2016
//
//Purpose: Manipulate DNA
//***************************

import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class ManipulateDNA{
public static void main(String[] args){

Scanner scan = new Scanner(System.in);
Random r = new Random();

System.out.println("Shick, Brindle\nLab 6\n" + new Date() + "\n");

//Prompts the user for input, and saves it as 'dnaString'
System.out.println("Enter a string of DNA (with no spaces): ");
String dnaString;
dnaString = scan.nextLine();

//Converts to uppercase
dnaString = dnaString.toUpperCase();
System.out.println("Your DNA string is: " + dnaString);

//Making complement 
String compDNA1 = dnaString.replace("A","W").replace("T","X").replace("G","Y").replace("C","Z");
String compDNA2 = compDNA1.replace("W","T").replace("X","A").replace("Y","C").replace("Z","G");
System.out.println("Complement = " + compDNA2);

//Mutations
Random ran = new Random();

//Declarations
String s1a, s2a, s1b, s2b, s1c, s2c;
int length, len1, len2, len3;
int location1, location2, location3;
char mutation1, mutation2, mutation3; 

//Determining length and random mutation points
length = dnaString.length();
location1 = ran.nextInt(length+1);
location2 = ran.nextInt(length+1);
location3 = ran.nextInt(length+1);

s1a = dnaString.substring(0,location1); 
s2a = dnaString.substring(location1);

s1b = dnaString.substring(0,location2);
s2b = dnaString.substring(location2);

s1c = dnaString.substring(0,location3);
s2c = dnaString.substring(location3);

mutation1 = "ATCG".charAt(ran.nextInt(4));
mutation2 = "ATCG".charAt(ran.nextInt(4));
mutation3 = "ATCG".charAt(ran.nextInt(4));

len1 = s1a.length();
len2 = s1b.length();
len3 = s1c.length(); 
	
//Output
System.out.println("Mutation 1: " + s1a.substring(0,len1-1) + mutation1 + s2a); 
System.out.println("Mutation 2: " + s1b + mutation2 + s2b);
System.out.println("Mutation 3: " + s1c.substring(0,len3-1) + s2c);
 }
}

